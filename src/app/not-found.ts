import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>We cannot open this page!</h1>`
})
export class NotFoundComponent{

}
