import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.css']
})
export class DishesComponent implements OnInit {
@Input() section!: string ;
@Input() sectionName1!: string ;
@Input() sectionName2!: string ;
@Input() sectionName3!: string ;

  constructor() { }

  ngOnInit(): void {
  }

}
