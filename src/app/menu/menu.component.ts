import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menu = [
    {section: 'Горячие закуски',name1: 'Пирог с курицей', name2: 'Пирог с грибами', name3: 'Пирог с говядиной' },
    {section: 'Салаты',name1: 'Оливье', name2: 'Винегрет', name3: 'Селедка под шубой' },
    {section: 'Первые блюда',name1: 'Шорпо', name2: 'Борщ', name3: 'Суп с фрикадельками' },
    {section: 'Вторые блюда',name1: 'Бризоль', name2: 'Фрикассе', name3: 'Бефстроганов' },
    {section: 'Шашлыки',name1: 'Шашлык из курицы', name2: 'Шашлык из говядины', name3: 'Шашлык из баранины' },
    {section: 'Напитки',name1: 'Сок', name2: 'Чай', name3: 'Газированные напитки' },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
