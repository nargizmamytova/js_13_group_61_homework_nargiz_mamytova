import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { ContactsComponent } from './contacts/contacts.component';
import { DishComponent } from './home/dish/dish.component';
import { DishesComponent } from './menu/dishes/dishes.component';
import { ModalComponent } from './ui/modal/modal.component';
import { NotFoundComponent } from './not-found';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'menu', component: MenuComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', component: NotFoundComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    MenuComponent,
    ContactsComponent,
    DishComponent,
    DishesComponent,
    ModalComponent,
    NotFoundComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
