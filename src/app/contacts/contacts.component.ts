import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent{
  modalOpen = false;
  constructor(private router: Router) {
  }
  openCheckoutModal(){
    this.modalOpen = true;
  }
  closeCheckOutModal(){
    this.modalOpen = false
  }

  continue() {
    void this.router.navigate(['/2Gis']);
  }
}
