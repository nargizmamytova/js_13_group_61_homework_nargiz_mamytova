import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  popularMenu = [
    {name: 'Лагман', consist: 'Мясо, лапша, овощи, ....', price: '200'},
    {name: 'Лагман-босо', consist: 'Мясо,жареная лапша, овощи, ....', price: '220'},
    {name: 'Лагман-гуро', consist: 'Мясо,отдельно лапша, овощи, ....', price: '250'},
    {name: 'Лагман-инжир', consist: 'Мясо, лапша, овощи, ....', price: '270'},
    {name: 'Лагман-тохо', consist: 'Мясо, грибы,лапша, овощи, ....', price: '270'}
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
