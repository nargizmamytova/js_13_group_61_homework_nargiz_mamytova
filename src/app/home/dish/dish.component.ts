import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.css']
})
export class DishComponent implements OnInit {
@Input() dishName!: string;
@Input() dishConsist!: string;
@Input() dishPrice!: string;
  constructor() { }

  ngOnInit(): void {
  }

}
